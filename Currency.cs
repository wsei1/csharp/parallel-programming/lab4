﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace LAB4
{
	public class Currency
	{
		public string Table { get; set; }
		[JsonProperty("currency")]
		public string CurrencyName { get; set; }
		public string Code { get; set; }
		public List<Rate> Rates { get; set; }
	}
}
