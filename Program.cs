﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace LAB4
{
	class Program
	{
		private const string URL_USD_RATE = "http://api.nbp.pl/api/exchangerates/rates/a/usd/last/30/?format=json";
		private const string URL_GOLD_RATES = "http://api.nbp.pl/api/cenyzlota/last/30/?format=json";
		private const string CURRENCIES = "http://api.nbp.pl/api/exchangerates/tables/a/?format=json";
		static async Task Main(string[] args)
		{
			CurrentCurrency currentCurrency;
			string currencyString = GenerateStringFromJson(URL_USD_RATE);
			string currencyGoldRatesString = GenerateStringFromJson(URL_GOLD_RATES);
			string availableCurrenciesString = GenerateStringFromJson(CURRENCIES);

			Task<CurrentCurrency> taskCurrency = Task.Run(() => GetCurrentCurrency(currencyString));
			Task<List<GoldRate>> taskGoldRates = Task.Run(() => GetGoldRates(currencyGoldRatesString));
			Task<List<CurrencyTable>> taskCurrencyTable = Task.Run(() => GetAvailableCurrencies(availableCurrenciesString));

			var goldRateTask = await Task.Factory.ContinueWhenAll(new Task[] { taskCurrency, taskGoldRates }, (values) =>
			{
				var currency = (values[0] as Task<CurrentCurrency>).Result;
				var goldRates = (values[1] as Task<List<GoldRate>>).Result;
				return CalculateGoldRatePerCurrency(goldRates, currency);
			});

			var goldRates = await taskGoldRates;
			var availableCurrencies = await taskCurrencyTable;
			currentCurrency = await taskCurrency;

			Console.WriteLine("--------------------------------------------");
			Console.WriteLine("Zadanie nr 1 - WYŚWIETLANIE KURSU ZŁOTA DLA USD");
			Console.WriteLine("--------------------------------------------");
			Console.WriteLine($"Obecny kurs złota w walucie {currentCurrency.Code}");
			Console.WriteLine("--------------------------------------------");
			// wyniki zadania 1
			DisplayResults(goldRateTask);

			Console.WriteLine("--------------------------------------------");
			Console.WriteLine("Lista dostępnych walut:");
			Console.WriteLine("--------------------------------------------");
			DisplayAvailableCurrencies(availableCurrencies);
			Console.WriteLine("--------------------------------------------");
			Console.WriteLine("Wybierz walute wpisując jej kod poniżej.");
			var choice = Console.ReadLine();
			Console.WriteLine("--------------------------------------------");
			if (availableCurrencies.First().Rates.FirstOrDefault(x => x.Code == choice) == null)
			{
				Console.WriteLine("Waluta o takim kodzie nie jest dostępna");
			} 
			else
			{
				var prepareUrl = GenerateStringFromJson("http://api.nbp.pl/api/exchangerates/rates/a/" + choice + "/last/30/?format=json");
				taskCurrency = Task.Run(() => GetCurrentCurrency(prepareUrl));
				currentCurrency = await taskCurrency;
				Console.WriteLine($"Wybrałeś {currentCurrency.CurrencyName}");
				Console.WriteLine("--------------------------------------------");

				Console.WriteLine($"Obecny kurs PLN na {currentCurrency.Code}");
				Console.WriteLine("--------------------------------------------");

				DisplayCurrency(currentCurrency);

				Console.WriteLine("--------------------------------------------");
				Console.WriteLine($"Obecny kurs złota w walucie {currentCurrency.Code}");
				Console.WriteLine("--------------------------------------------");
				goldRateTask = await Task.Factory.ContinueWhenAll(new Task[] { taskCurrency, taskGoldRates }, (values) =>
				{
					var currency = (values[0] as Task<CurrentCurrency>).Result;
					var goldRates = (values[1] as Task<List<GoldRate>>).Result;
					return CalculateGoldRatePerCurrency(goldRates, currency);
				});
				Console.WriteLine($"Zadanie nr 2 - WYŚWIETLANIE KURSU ZŁOTA DLA WYZNACZONEJ WALUTY {currentCurrency.Code}");
				Console.WriteLine("--------------------------------------------");
				// wyniki zadania 2
				DisplayResults(goldRateTask);
			}

			Console.ReadKey();
		}

		private static CurrentCurrency GetCurrentCurrency(string json)
		{
			return JsonConvert.DeserializeObject<CurrentCurrency>(json);
		}

		private static List<GoldRate> GetGoldRates(string json)
		{
			return JsonConvert.DeserializeObject<List<GoldRate>>(json);
		}

		private static List<CurrencyTable> GetAvailableCurrencies(string json)
		{
			return JsonConvert.DeserializeObject<List<CurrencyTable>>(json);
		}

		private static void DisplayResults(List<GoldRateDisplayHelper> goldRateInSpecificCurrency)
		{
			var lowestRate = goldRateInSpecificCurrency.Min(x => x.Decimal);
			var highestRate = goldRateInSpecificCurrency.Max(x => x.Decimal);
			var todayRate = goldRateInSpecificCurrency.First(x => x.DateTime == DateTime.Today).Decimal;

			Console.WriteLine($"Najniższy kurs złota w walucie {goldRateInSpecificCurrency.First().Currency.Code} " +
			$"to {Math.Round(lowestRate, 2)} dn. {goldRateInSpecificCurrency.First(x => x.Decimal == lowestRate).DateTime.ToShortDateString()}");

			Console.WriteLine($"Najwyższy kurs złota w walucie {goldRateInSpecificCurrency.First().Currency.Code} " +
			$"to {Math.Round(highestRate, 2)} dn. {goldRateInSpecificCurrency.First(x => x.Decimal == highestRate).DateTime.ToShortDateString()}");

			Console.WriteLine($"Rożnica pomiędzy najwyższym i " +
			$"dzisiejszym kursem złota w walucie {goldRateInSpecificCurrency.First().Currency.Code} wynosi {Math.Round(Math.Abs(todayRate - highestRate), 2)}");

			Console.WriteLine($"Rożnica pomiędzy najniższym i " +
			$"dzisiejszym kursem złota w walucie {goldRateInSpecificCurrency.First().Currency.Code} wynosi {Math.Round(Math.Abs(todayRate - lowestRate), 2)}");
		}

		private static string GenerateStringFromJson(string url)
		{
			HttpClient client = new HttpClient();
			HttpResponseMessage responseMessage = client.GetAsync(url).Result;
			responseMessage.EnsureSuccessStatusCode();
			return responseMessage.Content.ReadAsStringAsync().Result;
		}

		private static void DisplayCurrency(CurrentCurrency currency)
		{
			Console.WriteLine($"Tabela: {currency.Table}" +
				$"\nNazwa waluty: {currency.CurrencyName}" +
				$"\nKod waluty: {currency.Code}");
			Console.WriteLine("Rates:");
			foreach (var currentCurrency in currency.Rates)
			{
				Console.WriteLine($"No: {currentCurrency.No}" +
					$" EffectiveDate: {currentCurrency.EffectiveDate.ToShortDateString()}" +
					$" Mid: {currentCurrency.Mid}");
			}
		}

		private static void DisplayAvailableCurrencies(List<CurrencyTable> currencyTables)
		{
			foreach (var currencyTable in currencyTables)
			{
				foreach (var currency in currencyTable.Rates)
				{
					Console.WriteLine($"Nazwa: {currency.Currency} | Kod: {currency.Code}");
				}
			}
		}

		private static List<GoldRateDisplayHelper> CalculateGoldRatePerCurrency(List<GoldRate> goldRates, CurrentCurrency currency)
		{
			List<GoldRateDisplayHelper> list = new List<GoldRateDisplayHelper>();
			foreach (var goldRate in goldRates)
			{
				var mid = currency.Rates.Where(x => x.EffectiveDate.Equals(goldRate.Data)).First().Mid;
				var result = goldRate.Cena / mid;
				list.Add(new GoldRateDisplayHelper(currency, goldRate.Data, result));
			}

			// Komentuję kod, który w przejrzysty sposób wypisuje obecne kursy złota w danej walucie.
			// Nie jest to wymagane, ale ułatwia przeglądanie danych.
			//foreach (var element in list)
			//{
			//	Console.WriteLine($"Waluta: {currency.Code} | Data: {element.Item2.ToShortDateString()} | Obecny kurs złota w {currency.Code}: {Math.Round(element.Item3, 2)}");
			//}
			return list;
		}
	}
}
