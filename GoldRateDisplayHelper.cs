﻿using System;

namespace LAB4
{
	public class GoldRateDisplayHelper
	{
		public GoldRateDisplayHelper(CurrentCurrency currency, DateTime dateTime, decimal @decimal)
		{
			Currency = currency;
			DateTime = dateTime;
			Decimal = @decimal;
		}

		public CurrentCurrency Currency { get; set; }
		public DateTime DateTime { get; set; }
		public decimal Decimal { get; set; }
	}
}
