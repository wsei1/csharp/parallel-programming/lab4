﻿namespace LAB4
{
	public class CurrencyTableRate
	{
		public string Currency { get; set; }
		public string Code { get; set; }
		public decimal Mid { get; set; }
	}
}
