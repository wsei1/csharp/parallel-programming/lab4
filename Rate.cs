﻿using System;

namespace LAB4
{
	public class Rate
	{
		public string No { get; set; }
		public DateTime EffectiveDate { get; set; }
		public decimal Mid { get; set; }
	}
}
