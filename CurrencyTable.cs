﻿using System;
using System.Collections.Generic;

namespace LAB4
{
	public class CurrencyTable
	{
		public string Table { get; set; }
		public string No { get; set; }
		public DateTime EffectiveDate { get; set; }
		public List<CurrencyTableRate> Rates { get; set; }
	}
}
